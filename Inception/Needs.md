﻿Core Needs


* Needs to be able to upload/input items to track
* Needs to be able to track individual items for lookup
* Needs to be able to track items in multiple locations
* Needs to be able to download/export data


Core Requirements


* Lightweight, needs to not consume large amounts of resource to improve battery life.
* Should be easy to use, even for a beginner
* Should be quick to add/remove items from the inventory/lists
* Needs to be secure in both data security and data integrity
* Should employ a minimal navigation structure that puts the user a short distance from any function


High Level Architecture


App will be designed for iOS device only at this point.
Standard output files will be used to allow for future cross platform development.


Entire app hosted completely on the phone, no internet required.


User will interact with the App, which will use SQLite built into iOS to manage and store data.


Data Design

-See DataDesign.jpg
  
![alt text](DataDesign.jpg "Data Design")


User can add Items.
User can add Locations.
User can add an existing item to an existing location.
For each location there is a unique inventory list.
For each item there is a unique inventory list.