
For those who need to manage inventory counts, location, and price. MyInventory is a publically available mobile aplication on iOS devices 
that will contain all that is needed to update, track, and manage an inventory of any size. This will be a valuable resource for the indivuals
who manage such inventories and the companies they represent. MyInventory will focus on inventory management for small scale operations but will scale
for use by all. MyInventory will allow the user to add new items to their inventory lists, update the quantity of those items, determine the location
of those items, and adjust the cost and/or price of each item. This application will assist companies in keeping their inventory clean and accessible
even when in multiple locations. Unlike other inventory tracking sytems on the market, this application will focus on being lightweight and easy to
use so that any business owner, or inventory manager, can download and start using the application quickly with few learning barriers.